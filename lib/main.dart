import 'package:flutter/material.dart';

void main() {
  runApp(MyBeachApp());
}

//Grid List Example
class MyBeachApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: beaches(title: 'Beach Destinations'),
    );
  }
}

//Grid List Example
class MyEuropeApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: europe(title: 'European Destinations'),
    );
  }
}

//Grid List Example
class MyAmericaApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: america(title: 'South American Destinations'),
    );
  }
}

class beaches extends StatelessWidget {
  final String title;

  beaches({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title)),
      body: Scaffold(
        body: GridView.count(
          primary: false,
          padding: const EdgeInsets.all(20),
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          crossAxisCount: 2,
          childAspectRatio: 200 / 200,

          children: <Widget>[
            Container(
              child: fiji(),
            ),
            Container(
              child: maldives(),
            ),
            Container(
              child: borabora(),
            ),
            Container(
              child: tahiti(),
            ),
            Container(
              child: maui(),
            ),
            Container(
              child: usvirginislands(),
            ),
            Container(
              child: bahamas(),
            ),
            Container(
              child: kauai(),
            ),
          ],
        ),
      ),

      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                  color: Colors.blue,
                  image: DecorationImage(
                      image: AssetImage("images/vacation.jpg"),
                      fit: BoxFit.fitWidth)
              ),
            ),
            ListTile(
              title: Text('Vacation Destinations'),
            ),

            const Divider(
              color: Colors.black,
              height: 20,
              thickness: 5,
              indent: 20,
              endIndent: 20,
            ),

            ListTile(
              title: Text('Best Beaches in the World'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyBeachApp()),
                );
              },
            ),
            ListTile(
              title: Text('Best Places to Visit in Europe'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyEuropeApp()),
                );
              },
            ),
            ListTile(
              title: Text('Best Places to Visit in Central and South America'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyAmericaApp()),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

class europe extends StatelessWidget {
  final String title;

  europe({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title)),
      body: Scaffold(
        body: GridView.count(
          primary: false,
          padding: const EdgeInsets.all(20),
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          crossAxisCount: 2,
          childAspectRatio: 200 / 200,

          children: <Widget>[
            Container(
              child: paris(),
            ),
            Container(
              child: london(),
            ),
            Container(
              child: rome(),
            ),
            Container(
              child: florence(),
            ),
            Container(
              child: barcelona(),
            ),
            Container(
              child: prague(),
            ),
            Container(
              child: amsterdam(),
            ),
            Container(
              child: santorini(),
            ),
          ],
        ),
      ),

      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                  color: Colors.blue,
                  image: DecorationImage(
                      image: AssetImage("images/vacation.jpg"),
                      fit: BoxFit.fitWidth)
              ),
            ),
            ListTile(
              title: Text('Vacation Destinations'),
            ),

            const Divider(
              color: Colors.black,
              height: 20,
              thickness: 5,
              indent: 20,
              endIndent: 20,
            ),

            ListTile(
              title: Text('Best Beaches in the World'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyBeachApp()),
                );
              },
            ),
            ListTile(
              title: Text('Best Places to Visit  Europe'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyEuropeApp()),
                );
              },
            ),
            ListTile(
              title: Text('Best Places to Visit in Central and South America'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyAmericaApp()),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

class america extends StatelessWidget {
  final String title;

  america({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title)),
      body: Scaffold(
        body: GridView.count(
          primary: false,
          padding: const EdgeInsets.all(20),
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          crossAxisCount: 2,
          childAspectRatio: 200 / 200,

          children: <Widget>[
            Container(
              child: argentinepatagonia(),
            ),
            Container(
              child: machupicchu(),
            ),
            Container(
              child: riodejaneiro(),
            ),
            Container(
              child: costarica(),
            ),
            Container(
              child: chileanpatagonia(),
            ),
            Container(
              child: galapagosislands(),
            ),
            Container(
              child: cusco(),
            ),
            Container(
              child: torresdelpaine(),
            ),
          ],
        ),
      ),

      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                  color: Colors.blue,
                  image: DecorationImage(
                      image: AssetImage("images/vacation.jpg"),
                      fit: BoxFit.fitWidth)
              ),
            ),
            ListTile(
              title: Text('Vacation Destinations'),
            ),

            const Divider(
              color: Colors.black,
              height: 20,
              thickness: 5,
              indent: 20,
              endIndent: 20,
            ),

            ListTile(
              title: Text('Best Beaches in the World'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyBeachApp()),
                );
              },
            ),
            ListTile(
              title: Text('Best Places to Visit in Europe'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyEuropeApp()),
                );
              },
            ),
            ListTile(
              title: Text('Best Places to Visit in Central and South America'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyAmericaApp()),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

class fiji extends StatelessWidget {
  fiji({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:
      Container(
        width: 200,
        height: 200,
        child:
        InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            showfiji(context);
          },
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/fiji.jpg"), fit: BoxFit.fill)),
            width: 100,
            height: 100,
            alignment: Alignment.bottomLeft,
          ),
        ),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

showfiji(BuildContext context) {

  AlertDialog alert = AlertDialog(
    title: Text(
      'Fiji',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
    content:

    Container(
      child:
      Text(
        ("Boasting 333 islands, resplendent resorts, sparkling waters and endless stretches of pristine sand, Fiji is the perfect spot for paradise-seeking beachgoers. Travelers who want to snorkel while on vacation should set their sights on Taveuni's diverse coral reefs. Meanwhile, those pining for the tropical beaches featured in the movie 'Cast Away' and multiple seasons of the TV show 'Survivor' can catch a ferry from Port Denarau and head directly to the 20 islands that make up the Mamanuca Islands. Popular options include Qalito Island and Vomo Island."),
        textAlign: TextAlign.center,
      ),
    ),

    actions: [
      Column(
        children: <Widget>[
          Image.asset('images/fiji.jpg')
        ],
      )
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class bahamas extends StatelessWidget {
  bahamas({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:
      Container(
        width: 200,
        height: 200,
        child:
        InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            showbahamas(context);
          },
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/bahamas.jpg"), fit: BoxFit.fill)),
            width: 100,
            height: 100,
            alignment: Alignment.bottomLeft,
          ),
        ),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

showbahamas(BuildContext context) {

  AlertDialog alert = AlertDialog(
    title: Text(
      'Bahamas',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
    content:

    Container(
      child:
      Text(
        ("With countless beaches to choose from on 700 islands, it's easy for you to find your perfect spot in the Bahamas. From massive resorts like Atlantis, Paradise Island to privately owned islands, the variety of beaches here run the gamut. Pink Sand Beach on Harbour Island will wow you with its rosy hue, while The Exumas will delight you with their wild swimming pigs. If you're after underwater caves, diving excursions and a stretch of sand featured in the 'Pirates of the Caribbean' films, look no further than Gold Rock Beach in Lucayan National Park on Grand Bahama Island."),
        textAlign: TextAlign.center,
      ),
    ),

    actions: [
      Column(
        children: <Widget>[
          Image.asset('images/bahamas.jpg')
        ],
      )
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class borabora extends StatelessWidget {
  borabora({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:
      Container(
        width: 200,
        height: 200,
        child:
        InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            showborabora(context);
          },
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/borabora.jpg"), fit: BoxFit.fill)),
            width: 100,
            height: 100,
            alignment: Alignment.bottomLeft,
          ),
        ),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

showborabora(BuildContext context) {

  AlertDialog alert = AlertDialog(
    title: Text(
      'Bora Bora',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
    content:

    Container(
      child:
      Text(
        ("This French Polynesian island is small but mighty. Measuring only 6 miles long and less than 3 miles wide, every inch of Bora Bora is packed with stunning beaches, green jungles and ritzy resorts with overwater bungalows. Matira Beach is ideal for sunbathing and strolling along sandy shores, while the beach at the Four Seasons Resort Bora Bora is where you should head for Mount Otemanu views and calm water. Snorkeling and shark-feeding excursions are also available if you enjoy adventurous pursuits. "),
        textAlign: TextAlign.center,
      ),
    ),

    actions: [
      Column(
        children: <Widget>[
          Image.asset('images/borabora.jpg')
        ],
      )
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class kauai extends StatelessWidget {
  kauai({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:
      Container(
        width: 200,
        height: 200,
        child:
        InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            showkauai(context);
          },
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/kauai.jpg"), fit: BoxFit.fill)),
            width: 100,
            height: 100,
            alignment: Alignment.bottomLeft,
          ),
        ),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

showkauai(BuildContext context) {

  AlertDialog alert = AlertDialog(
    title: Text(
      'Kauai',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
    content:

    Container(
      child:
      Text(
        ("Adventurous travelers flock here in droves for the many heart-pumping excursions the island has to offer. However, Kauai's beaches should not be overlooked. Take a trip to Polihale State Park to hit secluded shorelines that overlook the island's jaw-dropping Napali Coast cliffs. Or, relax on an easily accessible stretch of sand like Poipu Beach Park. Just keep in mind that swimming here can be challenging because of the rough waters, so pay attention to the lifeguards and posted signs."),
        textAlign: TextAlign.center,
      ),
    ),

    actions: [
      Column(
        children: <Widget>[
          Image.asset('images/kauai.jpg')
        ],
      )
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class maldives extends StatelessWidget {
  maldives({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:
      Container(
        width: 200,
        height: 200,
        child:
        InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            showmaldives(context);
          },
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/maldives.jpg"), fit: BoxFit.fill)),
            width: 100,
            height: 100,
            alignment: Alignment.bottomLeft,
          ),
        ),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

showmaldives(BuildContext context) {

  AlertDialog alert = AlertDialog(
    title: Text(
      'Maldives',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
    content:

    Container(
      child:
      Text(
        ("The Maldives is unlike any beach destination you'll experience. This tropical paradise features 22 ring-shaped atolls made up of approximately 1,200 islands, with each beach more beautiful than the last. You'll likely stick to the island your resort is on, but you can expect pearlescent sands, towering palm trees, incredibly blue waters and surreal sunsets at all of the beaches in the Maldives. Most public beaches require covering up, so if you do stray from your resort's sands, consider checking out the bikini-friendly beaches on Rasdhoo and Maafushi. "),
        textAlign: TextAlign.center,
      ),
    ),

    actions: [
      Column(
        children: <Widget>[
          Image.asset('images/maldives.jpg')
        ],
      )
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class maui extends StatelessWidget {
  maui({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:
      Container(
        width: 200,
        height: 200,
        child:
        InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            showmaui(context);
          },
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/maui.jpg"), fit: BoxFit.fill)),
            width: 100,
            height: 100,
            alignment: Alignment.bottomLeft,
          ),
        ),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

showmaui(BuildContext context) {

  AlertDialog alert = AlertDialog(
    title: Text(
      'Maui',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
    content:

    Container(
      child:
      Text(
        ("If you're looking for a taste of Hawaii's beautiful beaches without the crowds on Oahu, retreat to Maui. The island boasts impressive beaches both big and small, adult- and family-friendly, as well as the black sand shores at Waianapanapa State Park. Southern Maui is especially popular with families, as it is home to Wailea Beach, which is known for its mostly calm water, bustling resorts and highly regarded restaurants. Meanwhile, the North Shore attracts active travelers keen on taking advantage of the superb windsurfing conditions at Hookipa Beach Park and Kanaha Beach Park."),
        textAlign: TextAlign.center,
      ),
    ),

    actions: [
      Column(
        children: <Widget>[
          Image.asset('images/maui.jpg')
        ],
      )
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class tahiti extends StatelessWidget {
  tahiti({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:
      Container(
        width: 200,
        height: 200,
        child:
        InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            showtahiti(context);
          },
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/tahiti.jpg"), fit: BoxFit.fill)),
            width: 100,
            height: 100,
            alignment: Alignment.bottomLeft,
          ),
        ),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

showtahiti(BuildContext context) {

  AlertDialog alert = AlertDialog(
    title: Text(
      'Tahiti',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
    content:

    Container(
      child:
      Text(
        ("Tahiti should be high on your list if you like your beaches paired with gourmet French cuisine. The 118 islands that make up Tahiti are known for top-notch fare, verdant jungles, lavish resorts and sandy shores. The smaller section of Tahiti (known as Tahiti Iti) is more secluded than the larger Tahiti Nui, but both are worth seeing. Tahiti Iti's famous La Plage de Maui is popular for a reason (think: sparkling sand and crystal-clear water), but the popularity comes with crowds. Check out Papenoo Beach on Tahiti Nui for excellent surfing conditions and more breathing room. "),
        textAlign: TextAlign.center,
      ),
    ),

    actions: [
      Column(
        children: <Widget>[
          Image.asset('images/tahiti.jpg')
        ],
      )
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class usvirginislands extends StatelessWidget {
  usvirginislands({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:
      Container(
        width: 200,
        height: 200,
        child:
        InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            showusvirginislands(context);
          },
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/usvirginislands.jpg"), fit: BoxFit.fill)),
            width: 100,
            height: 100,
            alignment: Alignment.bottomLeft,
          ),
        ),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

showusvirginislands(BuildContext context) {

  AlertDialog alert = AlertDialog(
    title: Text(
      'U.S. Virgin Islands',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
    content:

    Container(
      child:
      Text(
        ("A trip to the U.S. Virgin Islands appeals to all types of travelers, as St. John, St. Thomas and St. Croix cater to different categories of beachgoers. Beach lovers who want to get away from the crowds should head straight to St. John, where the quieter sands of Honeymoon and Solomon beaches appeal to privacy-seeking couples. St. Thomas' photogenic Magens Bay is perfect for photo-ops (though other tourists might get in your shot), and St. Croix's Sandy Point National Wildlife Refuge is ideal for vacationers wanting to share the sand and surf with leatherback sea turtles."),
        textAlign: TextAlign.center,
      ),
    ),

    actions: [
      Column(
        children: <Widget>[
          Image.asset('images/usvirginislands.jpg')
        ],
      )
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class santorini extends StatelessWidget {
  santorini({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:
      Container(
        width: 200,
        height: 200,
        child:
        InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            showsantorini(context);
          },
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/santorini.jpg"), fit: BoxFit.fill)),
            width: 100,
            height: 100,
            alignment: Alignment.bottomLeft,
          ),
        ),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

showsantorini(BuildContext context) {

  AlertDialog alert = AlertDialog(
    title: Text(
      'Santorini',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
    content:

    Container(
      child:
      Text(
        ("Visit this Greek island for its abundance of diverse beaches. You'll find red sands at Red Beach and black sands at Kamari Beach. You should also save a day to travel to archaeological attractions like Ancient Thira and Ancient Akrotiri, where you'll discover ruins and abandoned settlements from the ninth and 17th centuries B.C. (respectively). No matter where you go, you'll encounter stunning views of the electric blue sea and whitewashed buildings Santorini is famous for. Santorini is also known for its exquisitely swanky hotels, so you'll want to save up to stay in one."),
        textAlign: TextAlign.center,
      ),
    ),

    actions: [
      Column(
        children: <Widget>[
          Image.asset('images/santorini.jpg')
        ],
      )
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class amsterdam extends StatelessWidget {
  amsterdam({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:
      Container(
        width: 200,
        height: 200,
        child:
        InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            showamsterdam(context);
          },
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/amsterdam.jpg"), fit: BoxFit.fill)),
            width: 100,
            height: 100,
            alignment: Alignment.bottomLeft,
          ),
        ),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

showamsterdam(BuildContext context) {

  AlertDialog alert = AlertDialog(
    title: Text(
      'Amsterdam',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
    content:

    Container(
      child:
      Text(
        ("There's more to Amsterdam than its notorious 'coffee shops' and Red Light District. Spend the day biking through the city's stylish streets before exploring noteworthy museums, such as the Van Gogh Museum, Verzetsmuseum and the Anne Frank House. Plan a picnic in Vondelpark for lunch, or opt for a boat tour along the city's many canals when it's time to rest your feet. Friendly locals and affordable hotels keep bringing travelers back, especially during the warmer months."),
        textAlign: TextAlign.center,
      ),
    ),

    actions: [
      Column(
        children: <Widget>[
          Image.asset('images/amsterdam.jpg')
        ],
      )
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class prague extends StatelessWidget {
  prague({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:
      Container(
        width: 200,
        height: 200,
        child:
        InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            showprague(context);
          },
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/prague.jpg"), fit: BoxFit.fill)),
            width: 100,
            height: 100,
            alignment: Alignment.bottomLeft,
          ),
        ),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

showprague(BuildContext context) {

  AlertDialog alert = AlertDialog(
    title: Text(
      'Prague',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
    content:

    Container(
      child:
      Text(
        ("A vacation in Prague will leave you feeling as if you've stepped into a fairy tale. Take in the Gothic architecture and vibrant atmosphere of Old Town Square, and be sure to tour Prague Castle (and the other attractions on its grounds, including St. Vitus Cathedral and the Royal Palace). Prague's affordability compared to other captivating destinations in Europe makes it a great place for travelers on a budget."),
        textAlign: TextAlign.center,
      ),
    ),

    actions: [
      Column(
        children: <Widget>[
          Image.asset('images/prague.jpg')
        ],
      )
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class barcelona extends StatelessWidget {
  barcelona({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:
      Container(
        width: 200,
        height: 200,
        child:
        InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            showbarcelona(context);
          },
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/barcelona.jpg"), fit: BoxFit.fill)),
            width: 100,
            height: 100,
            alignment: Alignment.bottomLeft,
          ),
        ),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

showbarcelona(BuildContext context) {

  AlertDialog alert = AlertDialog(
    title: Text(
      'Barcelona',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
    content:

    Container(
      child:
      Text(
        ("Barcelona's diverse architecture sets the city apart from other European destinations. Gaudí's Park Güell and La Sagrada Família are beyond impressive, as are Catedral de Barcelona (also known as La Seu), Montjuïc Castle and the many medieval buildings in the Gothic Quarter. When you tire of taking in the city's stunning architecture, relax on La Barceloneta beach, sample a smattering of tasty local tapas or sip sangria along Las Ramblas."),
        textAlign: TextAlign.center,
      ),
    ),

    actions: [
      Column(
        children: <Widget>[
          Image.asset('images/barcelona.jpg')
        ],
      )
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class florence extends StatelessWidget {
  florence({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:
      Container(
        width: 200,
        height: 200,
        child:
        InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            showflorence(context);
          },
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/florence.jpg"), fit: BoxFit.fill)),
            width: 100,
            height: 100,
            alignment: Alignment.bottomLeft,
          ),
        ),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

showflorence(BuildContext context) {

  AlertDialog alert = AlertDialog(
    title: Text(
      'Florence',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
    content:

    Container(
      child:
      Text(
        ("Travel to Firenze for an authentic taste of Italy. Florence boasts top-notch museums, quaint hotels, stunning architecture and mouthwatering cuisine. Must-dos include admiring Michelangelo's David at the Galleria dell'Accademia, taking in city and river views from the Ponte Vecchio and climbing to the top of the Duomo – Florence's most recognizable attraction. When hunger strikes, head to the Mercato Centrale Firenze to stock up on fresh meats and cheeses or sit down for pizza or gelato at an outdoor eatery."),
        textAlign: TextAlign.center,
      ),
    ),

    actions: [
      Column(
        children: <Widget>[
          Image.asset('images/florence.jpg')
        ],
      )
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class rome extends StatelessWidget {
  rome({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:
      Container(
        width: 200,
        height: 200,
        child:
        InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            showrome(context);
          },
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/rome.jpg"), fit: BoxFit.fill)),
            width: 100,
            height: 100,
            alignment: Alignment.bottomLeft,
          ),
        ),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

showrome(BuildContext context) {

  AlertDialog alert = AlertDialog(
    title: Text(
      'Rome',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
    content:

    Container(
      child:
      Text(
        ("Rome is a can't-miss spot on your trip to Europe. The aroma of fresh Italian cooking wafts through the alleys, and historical sites stand proudly at every turn. No visit to Italy's capital would be complete without checking out the Colosseum, St. Peter's Basilica, the Sistine Chapel and the awe-inspiring Trevi Fountain. If you have additional time, venture beyond the main sights to the Roman Forum, Trastevere and the Spanish Steps."),
        textAlign: TextAlign.center,
      ),
    ),

    actions: [
      Column(
        children: <Widget>[
          Image.asset('images/rome.jpg')
        ],
      )
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class london extends StatelessWidget {
  london({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:
      Container(
        width: 200,
        height: 200,
        child:
        InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            showlondon(context);
          },
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/london.jpg"), fit: BoxFit.fill)),
            width: 100,
            height: 100,
            alignment: Alignment.bottomLeft,
          ),
        ),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

showlondon(BuildContext context) {

  AlertDialog alert = AlertDialog(
    title: Text(
      'London',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
    content:

    Container(
      child:
      Text(
        ("Exploring the world-class British Museum, seeing a musical in the West End, touring the Tower of London and gorging on fish and chips or a Sunday roast at a local pub are all part of the London bucket list experience. However, London's high hotel prices can make budget travelers cringe. To save some money, book your accommodations far in advance or consider a vacation rental."),
        textAlign: TextAlign.center,
      ),
    ),

    actions: [
      Column(
        children: <Widget>[
          Image.asset('images/london.jpg')
        ],
      )
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class paris extends StatelessWidget {
  paris({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:
      Container(
        width: 200,
        height: 200,
        child:
        InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            showparis(context);
          },
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/paris.jpg"), fit: BoxFit.fill)),
            width: 100,
            height: 100,
            alignment: Alignment.bottomLeft,
          ),
        ),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

showparis(BuildContext context) {

  AlertDialog alert = AlertDialog(
    title: Text(
      'Paris',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
    content:

    Container(
      child:
      Text(
        ("Paris is filled with highly regarded museums, monuments and churches. You could easily spend your entire vacation admiring iconic sights like the Eiffel Tower, wandering through exhibits at the Louvre and strolling through the beautiful green space admiring flowers at Luxembourg Gardens. Still, you should save some time for people-watching and munching on fresh croissants at sidewalk cafes during the day. Once the sun sets, sit down for a decadent French meal with amazing wine."),
        textAlign: TextAlign.center,
      ),
    ),

    actions: [
      Column(
        children: <Widget>[
          Image.asset('images/paris.jpg')
        ],
      )
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class argentinepatagonia extends StatelessWidget {
  argentinepatagonia({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:
      Container(
        width: 200,
        height: 200,
        child:
        InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            showargentinepatagonia(context);
          },
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/argentinepatagonia.jpg"), fit: BoxFit.fill)),
            width: 100,
            height: 100,
            alignment: Alignment.bottomLeft,
          ),
        ),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

showargentinepatagonia(BuildContext context) {

  AlertDialog alert = AlertDialog(
    title: Text(
      'Argentine Patagonia',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
    content:

    Container(
      child:
      Text(
        ("Argentine Patagonia is a true treat for all travelers, not just those with a penchant for adventure. From its diverse wildlife (such as Magellanic penguins, Andean cats and southern elephant seals) to its majestic mountains and jaw-dropping glaciers, this region showcases Mother Nature at its best. Even the most amateur of photographers will appreciate the palette of incredible colors on display here. Can't-miss places for first-timers include the Argentine Lake District, Mount Fitz Roy and the Perito Moreno Glacier."),
        textAlign: TextAlign.center,
      ),
    ),

    actions: [
      Column(
        children: <Widget>[
          Image.asset('images/argentinepatagonia.jpg')
        ],
      )
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class machupicchu extends StatelessWidget {
  machupicchu({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:
      Container(
        width: 200,
        height: 200,
        child:
        InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            showmachupicchu(context);
          },
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/machupicchu.jpg"), fit: BoxFit.fill)),
            width: 100,
            height: 100,
            alignment: Alignment.bottomLeft,
          ),
        ),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

showmachupicchu(BuildContext context) {

  AlertDialog alert = AlertDialog(
    title: Text(
      'Machu Picchu',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
    content:

    Container(
      child:
      Text(
        ("Travelers in search of Instagram-worthy vistas and a once-in-a-lifetime adventure should consider visiting Machu Picchu. Nestled within a tropical Peruvian forest almost 8,000 feet above sea level, this UNESCO World Heritage Site can only be reached by hiking the 27-mile Inca Trail or taking a train to Aguas Calientes. Once on-site, visitors can learn about some of the theories surrounding Machu Picchu's existence as they explore its approximately 200 religious, ceremonial, astronomical and agricultural structures. When it's time to retire for the night, stay in nearby Cusco, which sits about 45 miles southeast of the Inca Trail trailhead."),
        textAlign: TextAlign.center,
      ),
    ),

    actions: [
      Column(
        children: <Widget>[
          Image.asset('images/machupicchu.jpg')
        ],
      )
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class riodejaneiro extends StatelessWidget {
  riodejaneiro({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:
      Container(
        width: 200,
        height: 200,
        child:
        InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            showriodejaneiro(context);
          },
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/riodejaneiro.jpg"), fit: BoxFit.fill)),
            width: 100,
            height: 100,
            alignment: Alignment.bottomLeft,
          ),
        ),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

showriodejaneiro(BuildContext context) {

  AlertDialog alert = AlertDialog(
    title: Text(
      'Rio de Janeiro',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
    content:

    Container(
      child:
      Text(
        ("With its scenic beaches, leafy mountains and lively nightlife, Rio de Janeiro makes for a fun getaway any time of year. Whether you're looking to attend the city's larger-than-life Carnival celebration (which takes place every February) or explore Brazil's gorgeous natural landscape, Rio has it all. While here, don't miss a visit to Christ the Redeemer, a 98-foot-tall statue of Jesus Christ that sits at the top of Mount Corcovado. After taking in the attraction's sweeping city views, head down to the coast for a stroll on world-famous Ipanema Beach."),
        textAlign: TextAlign.center,
      ),
    ),

    actions: [
      Column(
        children: <Widget>[
          Image.asset('images/riodejaneiro.jpg')
        ],
      )
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class costarica extends StatelessWidget {
  costarica({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:
      Container(
        width: 200,
        height: 200,
        child:
        InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            showcostarica(context);
          },
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/costarica.jpg"), fit: BoxFit.fill)),
            width: 100,
            height: 100,
            alignment: Alignment.bottomLeft,
          ),
        ),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

showcostarica(BuildContext context) {

  AlertDialog alert = AlertDialog(
    title: Text(
      'Costa Rica',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
    content:

    Container(
      child:
      Text(
        ("Costa Rica is a natural wonderland. The tropical country features volcanoes to hike and lush rainforests to traverse on foot or by zip line. Plus, it boasts a refreshingly laid-back culture. Relaxation-seekers can immerse themselves in the 'pura vida' lifestyle and bask at the beaches in Guanacaste or along the Nicoya Peninsula's roughly 80 miles of coast. No visit would be complete without walking around historic San José and trying traditional delicacies like picadillo de chicasquil, a hash-like dish made with tree spinach."),
        textAlign: TextAlign.center,
      ),
    ),

    actions: [
      Column(
        children: <Widget>[
          Image.asset('images/costarica.jpg')
        ],
      )
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class chileanpatagonia extends StatelessWidget {
  chileanpatagonia({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:
      Container(
        width: 200,
        height: 200,
        child:
        InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            showchileanpatagonia(context);
          },
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/chileanpatagonia.jpg"), fit: BoxFit.fill)),
            width: 100,
            height: 100,
            alignment: Alignment.bottomLeft,
          ),
        ),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

showchileanpatagonia(BuildContext context) {

  AlertDialog alert = AlertDialog(
    title: Text(
      'Chilean Patagonia',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
    content:

    Container(
      child:
      Text(
        ("Some of South America's most incredible scenery can be found in Chilean Patagonia. Must-visit destinations here include Tierra del Fuego, the southeasternmost point of Chile, and the penguin-filled Isla Magdalena. Additionally, the area is home to storied sites like Cape Horn – the southernmost tip of South America that requires years of experience to expertly sail around – as well as Porvenir, a small settlement known for its charming Victorian houses and picturesque surroundings."),
        textAlign: TextAlign.center,
      ),
    ),

    actions: [
      Column(
        children: <Widget>[
          Image.asset('images/chileanpatagonia.jpg')
        ],
      )
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class galapagosislands extends StatelessWidget {
  galapagosislands({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:
      Container(
        width: 200,
        height: 200,
        child:
        InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            showgalapagosislands(context);
          },
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/galapagosislands.jpg"), fit: BoxFit.fill)),
            width: 100,
            height: 100,
            alignment: Alignment.bottomLeft,
          ),
        ),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

showgalapagosislands(BuildContext context) {

  AlertDialog alert = AlertDialog(
    title: Text(
      'Galapagos Islands',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
    content:

    Container(
      child:
      Text(
        ("The Galápagos Islands are best known for their colorful creatures, ranging from giant tortoises to seals to penguins. The islands are also famous for helping Charles Darwin develop his theory of natural selection. Today, animals still reign supreme (of the more than 120 islands, islets and rocks that comprise the Galápagos archipelago, only four are inhabited by humans). Getting here will cost you, as the remote islands sit about 600 miles off of the coast of Ecuador, but the plentiful wildlife-viewing opportunities make the trip worthwhile."),
        textAlign: TextAlign.center,
      ),
    ),

    actions: [
      Column(
        children: <Widget>[
          Image.asset('images/galapagosislands.jpg')
        ],
      )
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class cusco extends StatelessWidget {
  cusco({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:
      Container(
        width: 200,
        height: 200,
        child:
        InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            showcusco(context);
          },
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/cusco.jpg"), fit: BoxFit.fill)),
            width: 100,
            height: 100,
            alignment: Alignment.bottomLeft,
          ),
        ),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

showcusco(BuildContext context) {

  AlertDialog alert = AlertDialog(
    title: Text(
      'Cusco',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
    content:

    Container(
      child:
      Text(
        ("Formerly the capital of the Inca Empire before it was invaded and occupied by Spanish conquistadors in the 16th century, Cusco, Peru, showcases a unique blend of Andean and Spanish cultures. In the Plaza de Armas (a large square in the historic downtown area that once represented the exact center of the Inca Empire), visitors will find two Spanish churches – Iglesia de La Compañía de Jesús and La Catedral. Cusco also features multiple museums that display various historical artifacts, including the Museo Inka and the Museo Chileno de Arte Precolombino."),
        textAlign: TextAlign.center,
      ),
    ),

    actions: [
      Column(
        children: <Widget>[
          Image.asset('images/cusco.jpg')
        ],
      )
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class torresdelpaine extends StatelessWidget {
  torresdelpaine({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:
      Container(
        width: 200,
        height: 200,
        child:
        InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            showtorresdelpaine(context);
          },
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/torresdelpaine.jpg"), fit: BoxFit.fill)),
            width: 100,
            height: 100,
            alignment: Alignment.bottomLeft,
          ),
        ),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

showtorresdelpaine(BuildContext context) {

  AlertDialog alert = AlertDialog(
    title: Text(
      'Torres del Paine National Park',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
    content:

    Container(
      child:
      Text(
        ("Reaching this remote national park in Chilean Patagonia isn't easy, but travelers say that beholding the ice fields of Grey Glacier and the soaring peaks of the Cuernos del Paine are more than worth the trip. And with 448,000-plus acres of diverse land to explore, visitors will find boredom near impossible. Some of Torres del Paine's most popular trails include the challenging W Trek (a multiday feat past stunning lakes, mountains and glaciers) and the easier Mirador Las Torres hike. Before venturing into the wild, be sure to stock up on essentials in a nearby city like Puerto Natales."),
        textAlign: TextAlign.center,
      ),
    ),

    actions: [
      Column(
        children: <Widget>[
          Image.asset('images/torresdelpaine.jpg')
        ],
      )
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
